This project is developed under DeepBlue 2018 Competition

Problem Statement: To detect and profile manufacturers (Maggi, Pepsi, Coke etc) of Plastic Waste from image.


* # Technology:
   

*  Manufacturer detection using RetinaNet.

*  Model is trained for 3 classes Coca Cola Bottles, Pepsi Bottles and Maggi

*  Folium is used for tracing plastic waste on map using geo tagging.

*  Bokeh is used for charts.
